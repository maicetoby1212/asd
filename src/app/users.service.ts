import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor( public http: HttpClient ) { }
  
  public findAll(){

    return this.http.get(environment.apiEndpoint + '/users');
    
  }
  public find(uid:string){
    return this.http.get(environment.apiEndpoint + '/users/' + uid); 
  }

  public setClaims(uid:string, claims:any){
   return this.http.put(environment.apiEndpoint + '/users/claims/' + uid, claims); 
  }

  public delete(uid:string){
    return this.http.delete(environment.apiEndpoint + '/users/' + uid); 
   }
  /* cuidado con las direcciones. */


}
