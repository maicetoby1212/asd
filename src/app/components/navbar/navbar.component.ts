import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UsersService } from 'src/app/users.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
 
  users : any;
  constructor(
    public auths: AuthenticationService,
    public auth: AngularFireAuth,
    public user: UsersService

  ) {
    this.user_point();
   }

  ngOnInit(): void {
  }

  user_point()
  {
    this.user.findAll().subscribe(
      (responseUser: any) =>
     {
      
       this.users= responseUser;
      
    }
    ),
      ( error: any) => {
      console.log(error);
      /*siempre crear error, para el endpoint */
    }
  }


  logout(){
    this.auths.logout();      
  }

}
