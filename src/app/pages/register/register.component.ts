import { Component, Input, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Input() action: string | undefined;
  email = 'maico@example.com';
  pass = '123456';

  constructor( 
    public auth: AngularFireAuth, 
    private routes : Router,
    public usersService: UsersService
                   
    ) { }



  ngOnInit(): void {
  }

  
  logout(){
    this.auth.signOut();
  }

  customLogin(){
    this.auth.signInWithEmailAndPassword(this.email, this.pass)
     .then(res=> {
      this.routes.navigate(['profile']);
       console.log(res);
    
       
    })
    .catch(err=> console.log('Error cl:',err));
  }
 
  register(){
    this.auth.createUserWithEmailAndPassword(this.email, this.pass)
    .then(user => { console.log(user);
      this.routes.navigate(['profile']);
    })
    .catch(err=> console.log("Error user", err)); 
  }

}
